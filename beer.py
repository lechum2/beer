import time

while True:
    beer = 99
    while beer > 0:
        print(str(beer) + ' bottles of beer on the wall, '\
            + str(beer) + ' bottles of beer.')
        time.sleep(1)
        beer -= 1
        print('Take one down and pass it around, '\
             + str(beer) + ' bottles of beer on the wall.')
        time.sleep(1)
        print()

    print('No more bottles of beer on the wall,\
         no more bottles of beer.') 
    time.sleep(1)
    print('Go to the store and buy some more,\
         99 bottles of beer on the wall.') 
    time.sleep(1)
    print()
    print()