﻿using System;
using System.Threading;

namespace beer
{
    class Program
    {
        static void Main(string[] args)
        {
            int bottlesNumber = 99;

            while (true)
            {
                while (bottlesNumber > 0)
                {
                    Console.Write(bottlesNumber + " bottles of beer on the wall, ");
                    Console.Write(bottlesNumber + " bottles of beer.");
                    Console.WriteLine();

                    Thread.Sleep(2000);

                    Console.Write("Take one down and pass it around, ");
                    bottlesNumber = bottlesNumber - 1;

                    Thread.Sleep(2000);

                    Console.Write(bottlesNumber + " bottles of beer on the wall.");
                    Console.WriteLine();
                    Console.WriteLine();

                    Thread.Sleep(3000);
                }

                Console.WriteLine("No more bottles of beer on the wall, no more bottles of beer.");
                Thread.Sleep(2000);
                bottlesNumber = 99;
                Console.WriteLine("Go to the store and buy some more, " + bottlesNumber + " bottles of beer on the wall!");
                Thread.Sleep(3000);
                Console.WriteLine();
                Console.WriteLine();
            }
        }
    }
}
